package service

import (
    "context"
    "gitee.com/WisdomClassroom/DisciplineStruct/glb"
    "gitee.com/WisdomClassroom/core"
    "gitee.com/WisdomClassroom/core/models"
    "gitee.com/WisdomClassroom/core/protobufs/v1/pb"
    "github.com/jinzhu/gorm"
    "time"
)

func (s *Service) DeleteDiscipline(_ context.Context, in *pb.DeleteDisciplineRequest) (*pb.DeleteDisciplineResponse, error) {
    token := core.NewToken()
    err := token.UnpackToken(in.AuthToken, &core.FlagTokenCert)
    if err != nil {
        return &pb.DeleteDisciplineResponse{Status: &pb.ResponseStatus{
            Code: pb.ResponseStatusCode_NotAuth, Error: err.Error(),
        },}, nil
    }

    needCheckIfDsplnExists := true

    if token.Type == core.UserRoleTypeCodeRoot {
        userOfReq := models.UserModel{
            Model: models.Model{
                UUID: token.UUID,
            },
            Type: core.UserRoleTypeCodeRoot,
        }
        err = glb.DB.Scopes(notDeleted).Where(&userOfReq).First(&userOfReq).Error
        if err == gorm.ErrRecordNotFound {
            return &pb.DeleteDisciplineResponse{Status: &pb.ResponseStatus{Code: pb.ResponseStatusCode_OtherError,
                Message: "用户没有权限",
            },}, nil
        } else if err != nil {
            glb.Logger.Error(err.Error())
            return deleteDisciplineRespSvrErr, nil
        }
    } else {
        // 检查是否为学院管理员
        collegeDisciplineBind := models.CollegeDisciplineBindModel{
            DisciplineUUID: in.WillDeleteUUID,
        }
        err = glb.DB.Scopes(notDeleted).Where(&collegeDisciplineBind).First(&collegeDisciplineBind).Error
        if err == gorm.ErrRecordNotFound {
            return &pb.DeleteDisciplineResponse{Status: &pb.ResponseStatus{Code: pb.ResponseStatusCode_OtherError,
                Message: "专业编号不存在",
            },}, nil
        } else if err != nil {
            glb.Logger.Error(err.Error())
            return deleteDisciplineRespSvrErr, nil
        }
        needCheckIfDsplnExists = false
        collegeMngrBindOfReq := models.CollegeManagerBindModel{
            CollegeUUID: collegeDisciplineBind.CollegeUUID,
            ManagerUUID: token.UUID,
        }
        err = glb.DB.Scopes(notDeleted).Where(&collegeMngrBindOfReq).First(&collegeMngrBindOfReq).Error
        if err == gorm.ErrRecordNotFound {
            return &pb.DeleteDisciplineResponse{Status: &pb.ResponseStatus{Code: pb.ResponseStatusCode_OtherError,
                Message: "用户没有权限或编号不存在",
            },}, nil
        } else if err != nil {
            glb.Logger.Error(err.Error())
            return deleteDisciplineRespSvrErr, nil
        }
    }

    if len(in.WillDeleteUUID) != 36 {
        return &pb.DeleteDisciplineResponse{Status: &pb.ResponseStatus{Code: pb.ResponseStatusCode_OtherError,
            Message: "专业编号不正确",
        },}, nil
    }

    if needCheckIfDsplnExists {
        dsplnWithUUID := models.DisciplineModel{
            Model: models.Model{
                UUID: in.WillDeleteUUID,
            },
        }
        err = glb.DB.Scopes(notDeleted).Where(&dsplnWithUUID).First(&dsplnWithUUID).Error
        if err == gorm.ErrRecordNotFound {
            return &pb.DeleteDisciplineResponse{Status: &pb.ResponseStatus{Code: pb.ResponseStatusCode_OtherError,
                Message: "专业编号不存在",
            },}, nil
        } else if err != nil {
            glb.Logger.Error(err.Error())
            return deleteDisciplineRespSvrErr, nil
        }
    }

    err = glb.DB.Transaction(func(tx *gorm.DB) error {
        model := models.Model{
            Deleted:    true,
            DeleteTime: time.Now().Unix(),
        }

        discipline := models.DisciplineModel{Model: model}
        err = glb.DB.Scopes(notDeleted).Where("uuid=?", in.WillDeleteUUID).Model(&discipline).Updates(discipline).Error
        if err != nil {
            return err
        }

        collegeDsplnBind := models.CollegeDisciplineBindModel{Model: model}
        err = tx.Scopes(notDeleted).Where("discipline_uuid=?", in.WillDeleteUUID).Model(&collegeDsplnBind).Updates(collegeDsplnBind).Error
        if err != nil {
            return err
        }

        return nil
    })
    if err != nil {
        glb.Logger.Error(err.Error())
        return deleteDisciplineRespSvrErr, nil
    }

    return &pb.DeleteDisciplineResponse{
        Status: &pb.ResponseStatus{Code: pb.ResponseStatusCode_Suc},
    }, nil
}
