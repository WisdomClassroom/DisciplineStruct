package service

import (
    "context"
    "gitee.com/WisdomClassroom/DisciplineStruct/glb"
    "gitee.com/WisdomClassroom/core"
    "gitee.com/WisdomClassroom/core/models"
    "gitee.com/WisdomClassroom/core/protobufs/v1/pb"
    "github.com/jinzhu/gorm"
    "time"
)

func (s *Service) SetDisciplineManager(_ context.Context, in *pb.SetDisciplineManagerRequest) (*pb.SetDisciplineManagerResponse, error) {
    token := core.NewToken()
    err := token.UnpackToken(in.AuthToken, &core.FlagTokenCert)
    if err != nil {
        return &pb.SetDisciplineManagerResponse{Status: &pb.ResponseStatus{
            Code: pb.ResponseStatusCode_NotAuth, Error: err.Error(),
        },}, nil
    }

    needCheckIfDsplnExists := true

    if token.Type == core.UserRoleTypeCodeRoot {
        userOfReq := models.UserModel{
            Model: models.Model{
                UUID: token.UUID,
            },
            Type: core.UserRoleTypeCodeRoot,
        }
        err = glb.DB.Scopes(notDeleted).Where(&userOfReq).First(&userOfReq).Error
        if err == gorm.ErrRecordNotFound {
            return &pb.SetDisciplineManagerResponse{Status: &pb.ResponseStatus{Code: pb.ResponseStatusCode_OtherError,
                Message: "用户没有权限",
            },}, nil
        } else if err != nil {
            glb.Logger.Error(err.Error())
            return setDisciplineManagerRespSvrErr, nil
        }
    } else {
        dsplnMngrBindOfReq := models.DisciplineManagerBindModel{
            DisciplineUUID: in.WillSetDisciplineUUID,
            ManagerUUID:    token.UUID,
        }
        err = glb.DB.Scopes(notDeleted).Where(&dsplnMngrBindOfReq).First(&dsplnMngrBindOfReq).Error
        if err == gorm.ErrRecordNotFound {
            // 检查是否为学院管理员
            collegeDisciplineBind := models.CollegeDisciplineBindModel{
                DisciplineUUID: in.WillSetDisciplineUUID,
            }
            err = glb.DB.Scopes(notDeleted).Where(&collegeDisciplineBind).First(&collegeDisciplineBind).Error
            if err == gorm.ErrRecordNotFound {
                return &pb.SetDisciplineManagerResponse{Status: &pb.ResponseStatus{Code: pb.ResponseStatusCode_OtherError,
                    Message: "专业编号不存在",
                },}, nil
            } else if err != nil {
                glb.Logger.Error(err.Error())
                return setDisciplineManagerRespSvrErr, nil
            }
            needCheckIfDsplnExists = false
            collegeMngrBindOfReq := models.CollegeManagerBindModel{
                CollegeUUID: collegeDisciplineBind.CollegeUUID,
                ManagerUUID: token.UUID,
            }
            err = glb.DB.Scopes(notDeleted).Where(&collegeMngrBindOfReq).First(&collegeMngrBindOfReq).Error
            if err == gorm.ErrRecordNotFound {
                return &pb.SetDisciplineManagerResponse{Status: &pb.ResponseStatus{Code: pb.ResponseStatusCode_OtherError,
                    Message: "用户没有权限或编号不存在",
                },}, nil
            } else if err != nil {
                glb.Logger.Error(err.Error())
                return setDisciplineManagerRespSvrErr, nil
            }
        } else if err != nil {
            glb.Logger.Error(err.Error())
            return setDisciplineManagerRespSvrErr, nil
        }
    }

    if len(in.WillSetDisciplineUUID) != 36 {
        return &pb.SetDisciplineManagerResponse{Status: &pb.ResponseStatus{Code: pb.ResponseStatusCode_OtherError,
            Message: "专业编号不正确",
        },}, nil
    }
    if len(in.WillSetManagerUUID) != 36 {
        return &pb.SetDisciplineManagerResponse{Status: &pb.ResponseStatus{Code: pb.ResponseStatusCode_OtherError,
            Message: "管理员编号不正确",
        },}, nil
    }

    if needCheckIfDsplnExists {
        disciplineWithUUID := models.DisciplineModel{
            Model: models.Model{
                UUID: in.WillSetDisciplineUUID,
            },
        }
        err = glb.DB.Scopes(notDeleted).Where(&disciplineWithUUID).First(&disciplineWithUUID).Error
        if err == gorm.ErrRecordNotFound {
            return &pb.SetDisciplineManagerResponse{Status: &pb.ResponseStatus{Code: pb.ResponseStatusCode_OtherError,
                Message: "专业编号不存在",
            },}, nil
        } else if err != nil {
            glb.Logger.Error(err.Error())
            return setDisciplineManagerRespSvrErr, nil
        }
    }

    userOfMngrOfArg := models.UserModel{
        Model: models.Model{
            UUID: in.WillSetManagerUUID,
        },
    }
    err = glb.DB.Scopes(notDeleted).Where(&userOfMngrOfArg).First(&userOfMngrOfArg).Error
    if err == gorm.ErrRecordNotFound {
        return &pb.SetDisciplineManagerResponse{Status: &pb.ResponseStatus{Code: pb.ResponseStatusCode_OtherError,
            Message: "管理员编号不存在",
        },}, nil
    } else if err != nil {
        glb.Logger.Error(err.Error())
        return setDisciplineManagerRespSvrErr, nil
    }

    dsplnMngrBindOfArg := models.DisciplineManagerBindModel{
        DisciplineUUID: in.WillSetDisciplineUUID,
        ManagerUUID:    in.WillSetManagerUUID,
    }
    err = glb.DB.Scopes(notDeleted).Where(&dsplnMngrBindOfArg).First(&dsplnMngrBindOfArg).Error
    if err == nil {
        return &pb.SetDisciplineManagerResponse{Status: &pb.ResponseStatus{Code: pb.ResponseStatusCode_OtherError,
            Message: "此管理员绑定组合已存在",
        },}, nil
    } else if err != gorm.ErrRecordNotFound {
        glb.Logger.Error(err.Error())
        return setDisciplineManagerRespSvrErr, nil
    }

    dsplnMngrBind := models.DisciplineManagerBindModel{
        Model: models.Model{
            UUID:         core.GenUUID(),
            CreationTime: time.Now().Unix(),
        },
        DisciplineUUID: in.WillSetDisciplineUUID,
        ManagerUUID:    in.WillSetManagerUUID,
    }
    err = glb.DB.Create(&dsplnMngrBind).Error
    if err != nil {
        glb.Logger.Error(err.Error())
        return setDisciplineManagerRespSvrErr, nil
    }

    return &pb.SetDisciplineManagerResponse{
        Status: &pb.ResponseStatus{Code: pb.ResponseStatusCode_Suc},
    }, nil
}
