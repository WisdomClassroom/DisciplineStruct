package service

import (
    "context"
    "gitee.com/WisdomClassroom/DisciplineStruct/glb"
    "gitee.com/WisdomClassroom/core"
    "gitee.com/WisdomClassroom/core/models"
    "gitee.com/WisdomClassroom/core/protobufs/v1/pb"
    "github.com/jinzhu/gorm"
)

func (s *Service) GetDiscipline(_ context.Context, in *pb.GetDisciplineRequest) (*pb.GetDisciplineResponse, error) {
    token := core.NewToken()
    err := token.UnpackToken(in.AuthToken, &core.FlagTokenCert)
    if err != nil {
        return &pb.GetDisciplineResponse{Status: &pb.ResponseStatus{
            Code: pb.ResponseStatusCode_NotAuth, Error: err.Error(),
        },}, nil
    }

    if len(in.DisciplineUUID) != 36 {
        return &pb.GetDisciplineResponse{Status: &pb.ResponseStatus{Code: pb.ResponseStatusCode_OtherError,
            Message: "专业编号不正确",
        },}, nil
    }

    discipline := models.DisciplineModel{
        Model: models.Model{
            UUID: in.DisciplineUUID,
        },
    }
    err = glb.DB.Scopes(notDeleted).Where(&discipline).First(&discipline).Error
    if err == gorm.ErrRecordNotFound {
        return &pb.GetDisciplineResponse{Status: &pb.ResponseStatus{Code: pb.ResponseStatusCode_OtherError,
            Message: "专业编号不存在",
        },}, nil
    } else if err != nil {
        glb.Logger.Error(err.Error())
        return getDisciplineRespSvrErr, nil
    }

    collegeDsplnBind := models.CollegeDisciplineBindModel{
        DisciplineUUID: in.DisciplineUUID,
    }
    err = glb.DB.Scopes(notDeleted).Where(&collegeDsplnBind).First(&collegeDsplnBind).Error
    if err != nil {
        glb.Logger.Error(err.Error())
        return getDisciplineRespSvrErr, nil
    }

    var disciplineManagerBinds []models.DisciplineManagerBindModel
    err = glb.DB.Scopes(notDeleted).Where("discipline_uuid=?", in.DisciplineUUID).Find(&disciplineManagerBinds).Error
    if err != nil && err != gorm.ErrRecordNotFound {
        glb.Logger.Error(err.Error())
        return getDisciplineRespSvrErr, nil
    }

    var managerUUIDs []string
    for _, model := range disciplineManagerBinds {
        managerUUIDs = append(managerUUIDs, model.ManagerUUID)
    }

    return &pb.GetDisciplineResponse{
        Status:      &pb.ResponseStatus{Code: pb.ResponseStatusCode_Suc},
        Name:        discipline.Name,
        Description: discipline.Description,
        CollegeUUID: collegeDsplnBind.CollegeUUID,
        ManagerUUID: managerUUIDs,
    }, nil
}
