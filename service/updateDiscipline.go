package service

import (
    "context"
    "gitee.com/WisdomClassroom/DisciplineStruct/glb"
    "gitee.com/WisdomClassroom/core"
    "gitee.com/WisdomClassroom/core/models"
    "gitee.com/WisdomClassroom/core/protobufs/v1/pb"
    "github.com/jinzhu/gorm"
    "time"
)

func (s *Service) UpdateDiscipline(_ context.Context, in *pb.UpdateDisciplineRequest) (*pb.UpdateDisciplineResponse, error) {
    token := core.NewToken()
    err := token.UnpackToken(in.AuthToken, &core.FlagTokenCert)
    if err != nil {
        return &pb.UpdateDisciplineResponse{Status: &pb.ResponseStatus{
            Code: pb.ResponseStatusCode_NotAuth, Error: err.Error(),
        },}, nil
    }

    needCheckIfDsplnExists := true

    if token.Type == core.UserRoleTypeCodeRoot {
        userOfReq := models.UserModel{
            Model: models.Model{
                UUID: token.UUID,
            },
            Type: core.UserRoleTypeCodeRoot,
        }
        err = glb.DB.Scopes(notDeleted).Where(&userOfReq).First(&userOfReq).Error
        if err == gorm.ErrRecordNotFound {
            return &pb.UpdateDisciplineResponse{Status: &pb.ResponseStatus{Code: pb.ResponseStatusCode_OtherError,
                Message: "用户没有权限",
            },}, nil
        } else if err != nil {
            glb.Logger.Error(err.Error())
            return updateDisciplineRespSvrErr, nil
        }
    } else {
        // 检查是否为专业管理员
        dsplnMngrBindOfReq := models.DisciplineManagerBindModel{
            DisciplineUUID: in.WillUpdateUUID,
            ManagerUUID:    token.UUID,
        }
        err = glb.DB.Scopes(notDeleted).Where(&dsplnMngrBindOfReq).First(&dsplnMngrBindOfReq).Error
        if err == gorm.ErrRecordNotFound {
            // 检查是否为学院管理员
            collegeDisciplineBind := models.CollegeDisciplineBindModel{
                DisciplineUUID: in.WillUpdateUUID,
            }
            err = glb.DB.Scopes(notDeleted).Where(&collegeDisciplineBind).First(&collegeDisciplineBind).Error
            if err == gorm.ErrRecordNotFound {
                return &pb.UpdateDisciplineResponse{Status: &pb.ResponseStatus{Code: pb.ResponseStatusCode_OtherError,
                    Message: "专业编号不存在",
                },}, nil
            } else if err != nil {
                glb.Logger.Error(err.Error())
                return updateDisciplineRespSvrErr, nil
            }
            needCheckIfDsplnExists = false
            collegeMngrBindOfReq := models.CollegeManagerBindModel{
                CollegeUUID: collegeDisciplineBind.CollegeUUID,
                ManagerUUID: token.UUID,
            }
            err = glb.DB.Scopes(notDeleted).Where(&collegeMngrBindOfReq).First(&collegeMngrBindOfReq).Error
            if err == gorm.ErrRecordNotFound {
                return &pb.UpdateDisciplineResponse{Status: &pb.ResponseStatus{Code: pb.ResponseStatusCode_OtherError,
                    Message: "用户没有权限或编号不存在",
                },}, nil
            } else if err != nil {
                glb.Logger.Error(err.Error())
                return updateDisciplineRespSvrErr, nil
            }
        } else if err != nil {
            glb.Logger.Error(err.Error())
            return updateDisciplineRespSvrErr, nil
        }
    }

    if len(in.NewName) == 0 {
        return &pb.UpdateDisciplineResponse{Status: &pb.ResponseStatus{Code: pb.ResponseStatusCode_OtherError,
            Message: "专业名称未填写",
        },}, nil
    }
    if len(in.WillUpdateUUID) != 36 {
        return &pb.UpdateDisciplineResponse{Status: &pb.ResponseStatus{Code: pb.ResponseStatusCode_OtherError,
            Message: "专业编号不正确",
        },}, nil
    }

    dsplnWithUUID := models.DisciplineModel{
        Model: models.Model{
            UUID: in.WillUpdateUUID,
        },
    }
    txDsplnWithUUID := glb.DB.Where(dsplnWithUUID)

    if needCheckIfDsplnExists {
        err = txDsplnWithUUID.Scopes(notDeleted).First(&dsplnWithUUID).Error
        if err == gorm.ErrRecordNotFound {
            return &pb.UpdateDisciplineResponse{Status: &pb.ResponseStatus{Code: pb.ResponseStatusCode_OtherError,
                Message: "专业编号不存在",
            },}, nil
        } else if err != nil {
            glb.Logger.Error(err.Error())
            return updateDisciplineRespSvrErr, nil
        }
    }

    discipline := models.DisciplineModel{
        Model: models.Model{
            UpdateTime: time.Now().Unix(),
        },
        Name:        in.NewName,
        Description: in.NewDescription,
    }
    err = txDsplnWithUUID.Model(&discipline).Updates(discipline).Error
    if err != nil {
        glb.Logger.Error(err.Error())
        return updateDisciplineRespSvrErr, nil
    }

    return &pb.UpdateDisciplineResponse{
        Status: &pb.ResponseStatus{Code: pb.ResponseStatusCode_Suc},
    }, nil
}
