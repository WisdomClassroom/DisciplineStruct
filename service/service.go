/*******************************************************************************
 * Copyright 2020 huanggefan.cn
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/

package service

import (
    "gitee.com/WisdomClassroom/core/protobufs/v1/pb"
    "github.com/jinzhu/gorm"
)

type Service struct{}

var getDisciplineRespSvrErr = &pb.GetDisciplineResponse{
    Status: &pb.ResponseStatus{Code: pb.ResponseStatusCode_ServerError},
}
var listDisciplineRespSvrErr = &pb.ListDisciplineResponse{
    Status: &pb.ResponseStatus{Code: pb.ResponseStatusCode_ServerError},
}
var listCollegeDisciplineRespSvrErr = &pb.ListCollegeDisciplineResponse{
    Status: &pb.ResponseStatus{Code: pb.ResponseStatusCode_ServerError},
}
var createDisciplineRespSvrErr = &pb.CreateDisciplineResponse{
    Status: &pb.ResponseStatus{Code: pb.ResponseStatusCode_ServerError},
}
var updateDisciplineRespSvrErr = &pb.UpdateDisciplineResponse{
    Status: &pb.ResponseStatus{Code: pb.ResponseStatusCode_ServerError},
}
var deleteDisciplineRespSvrErr = &pb.DeleteDisciplineResponse{
    Status: &pb.ResponseStatus{Code: pb.ResponseStatusCode_ServerError},
}
var setDisciplineManagerRespSvrErr = &pb.SetDisciplineManagerResponse{
    Status: &pb.ResponseStatus{Code: pb.ResponseStatusCode_ServerError},
}
var unsetDisciplineManagerRespSvrErr = &pb.UnSetDisciplineManagerResponse{
    Status: &pb.ResponseStatus{Code: pb.ResponseStatusCode_ServerError},
}

func notDeleted(db *gorm.DB) *gorm.DB {
    return db.Where("deleted=?", false)
}
