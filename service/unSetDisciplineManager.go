package service

import (
    "context"
    "gitee.com/WisdomClassroom/DisciplineStruct/glb"
    "gitee.com/WisdomClassroom/core"
    "gitee.com/WisdomClassroom/core/models"
    "gitee.com/WisdomClassroom/core/protobufs/v1/pb"
    "github.com/jinzhu/gorm"
    "time"
)

func (s *Service) UnSetDisciplineManager(_ context.Context, in *pb.UnSetDisciplineManagerRequest) (*pb.UnSetDisciplineManagerResponse, error) {
    token := core.NewToken()
    err := token.UnpackToken(in.AuthToken, &core.FlagTokenCert)
    if err != nil {
        return &pb.UnSetDisciplineManagerResponse{Status: &pb.ResponseStatus{
            Code: pb.ResponseStatusCode_NotAuth, Error: err.Error(),
        },}, nil
    }

    if token.Type == core.UserRoleTypeCodeRoot {
        userOfReq := models.UserModel{
            Model: models.Model{
                UUID: token.UUID,
            },
            Type: core.UserRoleTypeCodeRoot,
        }
        err = glb.DB.Scopes(notDeleted).Where(&userOfReq).First(&userOfReq).Error
        if err == gorm.ErrRecordNotFound {
            return &pb.UnSetDisciplineManagerResponse{Status: &pb.ResponseStatus{Code: pb.ResponseStatusCode_OtherError,
                Message: "用户没有权限",
            },}, nil
        } else if err != nil {
            glb.Logger.Error(err.Error())
            return unsetDisciplineManagerRespSvrErr, nil
        }
    } else {
        collegeDsplnBindWithUUID := models.CollegeDisciplineBindModel{
            DisciplineUUID: in.WillUnSetDisciplineUUID,
        }
        err = glb.DB.Scopes(notDeleted).Where(&collegeDsplnBindWithUUID).First(&collegeDsplnBindWithUUID).Error
        if err == gorm.ErrRecordNotFound {
            return &pb.UnSetDisciplineManagerResponse{Status: &pb.ResponseStatus{Code: pb.ResponseStatusCode_OtherError,
                Message: "专业编号不存在",
            },}, nil
        } else if err != nil {
            glb.Logger.Error(err.Error())
            return unsetDisciplineManagerRespSvrErr, nil
        }
        collegeMngrBindOfReq := models.CollegeManagerBindModel{
            CollegeUUID: collegeDsplnBindWithUUID.CollegeUUID,
            ManagerUUID: token.UUID,
        }
        err = glb.DB.Scopes(notDeleted).Where(&collegeMngrBindOfReq).First(&collegeMngrBindOfReq).Error
        if err == gorm.ErrRecordNotFound {
            return &pb.UnSetDisciplineManagerResponse{Status: &pb.ResponseStatus{Code: pb.ResponseStatusCode_OtherError,
                Message: "用户没有权限或编号不存在",
            },}, nil
        } else if err != nil {
            glb.Logger.Error(err.Error())
            return unsetDisciplineManagerRespSvrErr, nil
        }
    }

    if len(in.WillUnSetDisciplineUUID) != 36 {
        return &pb.UnSetDisciplineManagerResponse{Status: &pb.ResponseStatus{Code: pb.ResponseStatusCode_OtherError,
            Message: "专业编号不正确",
        },}, nil
    }
    if len(in.WillUnSetManagerUUID) != 36 {
        return &pb.UnSetDisciplineManagerResponse{Status: &pb.ResponseStatus{Code: pb.ResponseStatusCode_OtherError,
            Message: "管理员编号不正确",
        },}, nil
    }

    dsplnMngrBindOfArg := models.DisciplineManagerBindModel{
        DisciplineUUID: in.WillUnSetDisciplineUUID,
        ManagerUUID:    in.WillUnSetManagerUUID,
    }
    txDisciplineManagerBindUUID := glb.DB.Where(dsplnMngrBindOfArg)
    err = txDisciplineManagerBindUUID.Scopes(notDeleted).First(&dsplnMngrBindOfArg).Error
    if err == gorm.ErrRecordNotFound {
        return &pb.UnSetDisciplineManagerResponse{Status: &pb.ResponseStatus{Code: pb.ResponseStatusCode_OtherError,
            Message: "此管理员绑定组合不存在",
        },}, nil
    } else if err != nil {
        glb.Logger.Error(err.Error())
        return unsetDisciplineManagerRespSvrErr, nil
    }

    disciplineMngrBind := models.DisciplineManagerBindModel{
        Model: models.Model{
            Deleted:    true,
            DeleteTime: time.Now().Unix(),
        },
    }
    err = txDisciplineManagerBindUUID.Model(&disciplineMngrBind).Updates(disciplineMngrBind).Error
    if err != nil {
        glb.Logger.Error(err.Error())
        return unsetDisciplineManagerRespSvrErr, nil
    }

    return &pb.UnSetDisciplineManagerResponse{
        Status: &pb.ResponseStatus{Code: pb.ResponseStatusCode_Suc},
    }, nil
}
