module gitee.com/WisdomClassroom/DisciplineStruct

go 1.13

require (
	gitee.com/WisdomClassroom/core v0.6.1
	github.com/grpc-ecosystem/grpc-gateway v1.13.0
	github.com/jinzhu/gorm v1.9.12
	google.golang.org/grpc v1.27.1
)
